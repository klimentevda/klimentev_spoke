package login;


import mail.listner.Listener1;
import mail.ui.initdriver.Element;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import mail.logic.mail.User;
import mail.pages.Pages;

import static mail.logic.BaseTest.USER_EMAIL;

public class loginmail extends Listener1 {

    private User Denia = new User();
    private Element element = new Element();

    @BeforeMethod
    public void openMail() {
        Denia.OpenMail();
    }

    @AfterMethod
    public void outMail() {
        Denia.LeaveMail();
    }

    @Test
    public void userGoToMail() {

        Denia.loginIn();

        Assert.assertEquals(
                element.notSuchProtectedGetText(Pages.get().mailPage.tabs.incomingPage.getEmailUserLink()),
                USER_EMAIL, "User Email not Expected Email");

    }
}
