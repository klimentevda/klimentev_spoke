package mail.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Class to work with date time
 */
public class CustomDate {

    public enum TimePatterns{
        STANDARD_DATE_PATTERN("yyyy-MM-dd"),
        SIMPLE_DATE("dd/MM/yy"),
        FULL_DATE_TIME_PATTERN("yyyy-MM-dd HH:mm:ss");

        private String text;

        TimePatterns(String text){
            this.text = text;
        }

        public String getText() {
            return text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    private Date date;
    private String currentPattern = TimePatterns.SIMPLE_DATE.toString();

    public CustomDate(int year, int month, int day){
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.set(year, month, day);
        date = c.getTime();
    }

    public CustomDate(){
        date = new Date();
    }

    public CustomDate(String date){
        Date result;

        SimpleDateFormat time = new SimpleDateFormat(currentPattern);

        try {
            result = time.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        this.date = result;
    }

    public CustomDate(String date, String pattern){
        Date result;
        currentPattern = pattern;

        SimpleDateFormat time = new SimpleDateFormat(currentPattern);

        try {
            result = time.parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        this.date = result;
    }

    public CustomDate addDays(int amount){
        Calendar c = getCalendar();
        c.add(Calendar.DATE, amount);
        date = c.getTime();
        return this;
    }

    public CustomDate addMonths(int amount){
        Calendar c = getCalendar();
        c.add(Calendar.MONTH, amount);
        date = c.getTime();
        return this;
    }

    public CustomDate addYears(int amount){
        Calendar c = getCalendar();
        c.add(Calendar.YEAR, amount);
        date = c.getTime();
        return this;
    }

    public Date getDate(){
        return date;
    }

    public Calendar getCalendar(){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c;
    }

    public String getDate(String pattern){
        String result;
        SimpleDateFormat time = new SimpleDateFormat(pattern);
        result = time.format(date);
        return result;
    }

    public long getTimestamp(){
        return date.getTime();
    }

    @Deprecated
    public String getString(){
        return getDate(currentPattern);
    }

    @Override
    public String toString(){
        return getDate(currentPattern);
    }
}
