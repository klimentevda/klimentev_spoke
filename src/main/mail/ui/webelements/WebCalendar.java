package mail.ui.webelements;

import mail.core.utils.CustomDate;
import mail.ui.initdriver.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class WebCalendar extends Element {

    By prevoiousMonth = By.xpath(".//span[@title='Previous Month']");

    By nextMonth = By.xpath(".//span[@title='Next Month']");

    By selectedMonth = By.xpath(".//th[@title='Select Month']");

    //td[@data-day='11/25/2018']
    private final String DAY = ".//td[@data-day='%s']";

    private WebElement self;

    public WebCalendar(WebElement element){
        self = element;
    }

    public void selectData(CustomDate date){
        element.notSuchProtectedClick(self);
        String strDate = date.getDate("MM/dd/yyyy");

        WebElement button;
        if (date.getTimestamp() > new CustomDate().getTimestamp()) {
            button = self.findElement(nextMonth);
        }else{
            button = self.findElement(prevoiousMonth);
        }

        int retry = 25;
        while ((Element.findElement(self, DAY, strDate) == null) && (retry != 0)){
            retry--;
            button.click();
        }

        if (retry != 0)
            Element.findElement(self, DAY, strDate).click();

    }
}
