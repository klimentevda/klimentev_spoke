package mail.ui.webelements;

import mail.ui.initdriver.Element;
import mail.ui.initdriver.InitialDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Util class for working with table
 */
public class WebTable extends Element {

    private final By headerLocator;
    private final By rowLocator;
    private final By cellLocator;

    /** Element pointing on table **/
    private WebElement table;

    /**
     * Constructor with default table tags
     * @param table - element pointing on table
     */
    public WebTable(WebElement table){
        this.table = table;
        headerLocator = By.xpath(".//th");
        rowLocator = By.xpath(".//tr");
        cellLocator = By.xpath("./td");
    }

    /**
     * Constructor with custom table tags
     * @param table - element pointing on table
     * @param headerTag - header cell tag
     * @param rowTag - row tag
     * @param cellTag - cell tag
     */
    public WebTable(WebElement table, String headerTag, String rowTag, String cellTag){
        this.table = table;
        headerLocator = By.xpath(".//" + headerTag);
        rowLocator = By.xpath(".//" + rowTag);
        cellLocator = By.xpath(".//" + cellTag);
    }

    /**
     * @return element pointing on table
     */
    public WebElement get(){
        return table;
        /*
        return DriverUtils
                .getElementWait()
                .withMessage("Table " + table + " not visible")
                .until(ExpectedConditions.visibilityOf(table));
                */
    }

    /**
     * @return header cell locator
     */
    public By getHeaderLocator(){
        return headerLocator;
    }

    /**
     * @return row locator
     */
    public By getRowLocator() {
        return rowLocator;
    }

    /**
     * @return cell locator
     */
    public By getCellLocator() {
        return cellLocator;
    }

    /**
     * @return return list of headers in table
     */
    public List<WebElement> getHeaders(){
      return get().findElements(headerLocator);
    }

    /**
     * @return list of rows in table
     */
    public List<WebRow> getRows(){
        List<WebRow> rows = new ArrayList<>();
        get().findElements(rowLocator)
                .forEach(e ->{
                    if (e.isDisplayed())
                        rows.add(new WebRow(e, this));
                });

        return rows;
    }

    /**
     * Get index of column named in header or in first row
     * @param name - name of the column in header
     * @return - column index
     */
    public int getColumnIndex(String name){
        int column = 0;
        List<WebElement> headers = getHeaders();
        if (headers.size() == 0)
            headers = getRows().get(0).getCells();

        for(WebElement element : headers){
            if (element.getText()
                    .trim().toLowerCase()
                    .equals(name.toLowerCase()))
                break;
            column += 1;
        }

        if (column == headers.size()){
            throw new AssertionError("No column named '"+ name + "' found.");
        }

        return column;
    }

    /**
     * Get row by index
     * @param index
     * @return {@link WebRow} with index
     */
    public WebRow getRow(int index){
        return getRows().get(index);
    }

    /**
     * Get row that contains column with text
     * @param column - name of column
     * @param text - text expected in column
     * @return {@link WebRow}
     */
    public WebRow getRow(String column, String text){
        InitialDriver.getInstance();
        List<WebRow> rows = getRows();
        WebRow row = null;
        int index = getColumnIndex(column);
        for (WebRow element: rows){
            if (checkText(element.getCell(index).getText(),
                    text)){
                row = element;
                break;
            }
        }
        InitialDriver.getInstance();
        if (row == null){
            throw new AssertionError("Row from column '" + column + "' with value '" +text +"' not found");
        }

        return row;
    }

    /**
     * Get row that contains map.values in column named as map.keySet()
     * @param map
     * @return {@link WebRow}
     */
    public WebRow getRow(Map<String, String> map){
        List<WebRow> rows = getRows();
        WebRow row = null;

        for (WebRow element : rows) {
            boolean allConditionsClear = true;
            for (String key : map.keySet()) {
                int index = getColumnIndex(key);
                if (!checkText(
                        element.getCell(index).getText(),
                        map.get(key))) {
                    allConditionsClear = false;
                }
            }
            if (allConditionsClear){
                row = element;
                break;
            }
        }
        if (row == null){
            throw new AssertionError("Row with columns '" + map.keySet() + "' with values '" +map.values() +"' not found");
        }

        return row;
    }

    private boolean checkText(String cell, String value){
        return cell.contains(value);
    }
}
