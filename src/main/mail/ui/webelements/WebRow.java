package mail.ui.webelements;

import mail.ui.initdriver.InitialDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;


import java.util.List;

public class WebRow {

    private WebElement row;
    private WebTable table;

    public WebRow(WebElement element, WebTable table){
        row = element;
        this.table = table;
    }

    public WebElement get(){
        return InitialDriver.getElementWait().until(ExpectedConditions.visibilityOf(row));
    }

    public List<WebElement> getCells(){
        List<WebElement> cells = get().findElements(table.getCellLocator());
        if (cells.size() == 0){
            cells = get().findElements(table.getHeaderLocator());
        }
        return cells;
    }

    public WebElement getCell(String name){
        int index = table.getColumnIndex(name);
        return getCell(index);
    }

    public WebElement getCell(int index){
        List<WebElement> cells = getCells();
        if (cells.size() <= index){
            return get();
            //throw new RuntimeException("Element " + get() + " doesn't contain cell by index = " + index);
        }
        return cells.get(index);
    }
}
