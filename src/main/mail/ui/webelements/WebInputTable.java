package mail.ui.webelements;


import mail.ui.initdriver.Element;
import mail.ui.initdriver.InitialDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;


/**
 * Class that help to work with dropdown that consist from few components
 */
public class WebInputTable extends Element{

    /** Core web element **/
    private WebElement parrentDiv;

    private By inputLocator = By.xpath((".//input[@class='form-control tt-input']"));


    private String inputDivTemplate = ".//div[text() = '%s']";

    public String getAttributeText(String AttributeKey){
        return getInput().getAttribute(AttributeKey);
    }

    public WebInputTable(WebElement element){
        parrentDiv = element;
    }

    public WebElement get(){
        return InitialDriver.getElementWait().until(ExpectedConditions.visibilityOf(parrentDiv));
    }

    public WebElement getInput(){
        return get().findElement(inputLocator);
    }

    public WebElement getItem(String text){
        return element
                .waitUntilVisible(
                        get().findElement(By.xpath(String.format(inputDivTemplate, text))));
    }

    public void select(String text){
        element.notSuchProtectedClick(getInput());
        //TODO evil
        try {
            getItem(text).click();
        }catch (TimeoutException e){
            element.notSuchProtectedClick(getInput());
            getItem(text).click();
        }

    }
}
