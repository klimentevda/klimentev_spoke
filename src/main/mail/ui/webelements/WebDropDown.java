package mail.ui.webelements;


import mail.ui.initdriver.Element;
import mail.ui.initdriver.InitialDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;


import java.util.List;

/**
 * Class that help to work with dropdown that consist from few components
 */
public class WebDropDown extends Element {

    /** Core web element **/
    private WebElement parrentDiv;

    private By selectLocator = By.xpath(".//select");

    private By buttonLocator = By.xpath((".//*[@data-toggle='dropdown']"));

    private By dropdownItems = By.xpath(".//ul[@role='menu']/li");

    private String dropdownItemTemplate = ".//ul/li[normalize-space(.)='%s']";

    private String dropdownItemTemplatePartial = ".//ul/li[contains(normalize-space(.),'%s')]";

    public WebDropDown(WebElement element){
        parrentDiv = element;
    }

    public WebDropDown setButtonLocator(By buttonLocator) {
        this.buttonLocator = buttonLocator;
        return this;
    }

    public WebElement get(){
        return InitialDriver.getElementWait().until(ExpectedConditions.visibilityOf(parrentDiv));
    }

    public WebElement getButton(){
        return get().findElement(buttonLocator);
    }

    public List<WebElement> getItems(){
        element
                .waitUntilVisible(get().findElement(dropdownItems));
        return get().findElements(dropdownItems);
    }

    public String getText(){
        return getButton().getText();
    }

    public WebElement getItem(String text){
        return element
                .waitUntilVisible(
                        get().findElement(By.xpath(String.format(dropdownItemTemplate, text))));
    }

    public WebElement getItemByPartialText(String text){
        return element
                .waitUntilVisible(
                        get().findElement(By.xpath(String.format(dropdownItemTemplatePartial, text))));
    }

    public void select(String text){
        element.notSuchProtectedClick(getButton());
        //TODO evil
        try {
            getItem(text).click();
        }catch (TimeoutException e){
            element.notSuchProtectedClick(getButton());
            getItem(text).click();
        }

    }

    public void selectByPartialText(String text){
        element.notSuchProtectedClick(getButton());
        getItemByPartialText(text).click();
    }

    public void selectByComplexPath(String text){
        String[] path = text.split("|");

        element.notSuchProtectedClick(getButton());
        for (String webelement: path){
            element.mouseOver(getItem(webelement));
        }
        getItem(path[path.length-1]).click();
    }

    public void selectByIndex(int index){
        getButton().click();
        List<WebElement> list = getItems();
        if (list.size()<=index)
            throw new AssertionError("Select has no element indexed " + index);
        getItems().get(index).click();
    }
}
