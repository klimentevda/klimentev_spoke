package mail.ui.initdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;

public class Element {

    protected Element element = new Element();
    protected final WebDriver driver = InitialDriver.getInstance().getDriver();

    /**
     * Status of element
     * Get status of element
     *
     * @param webElementExpectedCondition WebElement element,By locator
     * @return getWebElement(ExpectedConditions.elementToBeClickable ( element));
     */
    public WebElement getWebElement(ExpectedCondition<WebElement> webElementExpectedCondition) {
        return waitElement().until(webElementExpectedCondition);
    }

    /**
     * Status of element
     * Get status of element
     *
     * @param stateElementExpectedCondition WebElement element,By locator
     * @return getWebStateOfElement(ExpectedConditions.elementToBeSelected ( element));
     */
    public boolean getWebStateOfElement(ExpectedCondition<Boolean> stateElementExpectedCondition) {
        return waitElement().until(stateElementExpectedCondition);
    }

    /**
     * It webDriver wait for all element
     *
     * @return
     */
    private WebDriverWait waitElement() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.pollingEvery(Duration.ofMillis(10));
        wait.ignoring(NoSuchElementException.class);
        wait.ignoring(StaleElementReferenceException.class);
        wait.ignoring(InterruptedException.class);
        return wait;
    }

    /**
     * @return WebDriverWait with element wait
     */
    public static WebDriverWait getElementWait() {
        WebDriverWait wait = new WebDriverWait(InitialDriver.getInstance().getDriver(),
                20);
        return wait;
    }


    public WebElement waitUntilClickable(By locator) {
        WebElement element = driver.findElement(By.id("trest"));
        return getWebElement(ExpectedConditions.elementToBeClickable(locator));
    }

    public WebElement waitUntilClickable(WebElement element) {
        return getWebElement(ExpectedConditions.elementToBeClickable(element));
    }

    public void notSuchProtectedClick(WebElement element) {
        try {
            element.click();
        } catch (NoSuchElementException e) {
            waitUntilClickable(element);
            element.click();
        }

    }

    public String notSuchProtectedGetText(WebElement element) {
        try {
            return element.getText();
        }catch (NoSuchElementException e){
            waitUntilClickable(element);
            return element.getText();
        }
    }

    public void notSuchProtectedSedKeys(WebElement element , String text) {
        try {
            element.sendKeys(text);
        }catch (NoSuchElementException e){
            waitUntilClickable(element);
            element.sendKeys(text);
        }
    }

    public WebElement waitUntilVisible(WebElement element) {
        return getWebElement(ExpectedConditions.visibilityOf(element));
    }

    public WebElement waitUntilVisible(By locator) {
        return getWebElement(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public WebElement waitUntilExist(By locator) {
        return getWebElement(ExpectedConditions.presenceOfElementLocated(locator));
    }


    public boolean isSelected(WebElement element) {
        return getWebStateOfElement(ExpectedConditions.elementToBeSelected(element));
    }

    public boolean isSelected(By locator) {
        return getWebStateOfElement(ExpectedConditions.elementToBeSelected(locator));
    }

    public boolean waitUntilInvisible(By locator) {
        return getWebStateOfElement(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public boolean waitUntilInvisible(WebElement element) {
        return getWebStateOfElement(ExpectedConditions.invisibilityOf(element));
    }

    public boolean waitUntilTextPresent(By locator, String text) {
        return getWebStateOfElement(ExpectedConditions.textToBe(locator, text));
    }

    public boolean waitUntilTextPresent(WebElement element, String text) {
        return getWebStateOfElement(ExpectedConditions.textToBePresentInElement(element, text));
    }


    public List<WebElement> selectFromList(By locator) {
        waitUntilClickable(locator);
        return driver.findElements(locator);
    }

    public static WebElement findElement(String pattern, Object... args) {
        By locator = By.xpath(String.format(pattern, args));
        try {
            return InitialDriver.currentDriver().findElement(locator);
        } catch (org.openqa.selenium.NoSuchElementException ex) {
            return null;
        }
    }

    public static WebElement findElement(WebElement element, String pattern, Object... args) {
        By locator = By.xpath(String.format(pattern, args));
        try {
            return element.findElement(locator);
        } catch (org.openqa.selenium.NoSuchElementException ex) {
            return null;
        }
    }
    public static void mouseOver(WebElement element){
        Actions actions = getActions();
        actions
                .moveToElement(element)
                .perform();
    }

    public static Actions getActions(){
        return new Actions(InitialDriver.currentDriver());
    }
}
