package mail.deserializers.property;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Properties {

	public String getPropValues(String name, String propertiesFileName)  {
		String result="";
		InputStream inputStream = null;
		try {
			java.util.Properties prop = new java.util.Properties();

			inputStream = getClass().getClassLoader().getResourceAsStream(propertiesFileName);

			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propertiesFileName + "' not found in the classpath");
			}

			result= prop.getProperty(name);
			inputStream.close();
		} catch (IOException e)
		{
			System.out.println("Exception: " + e );
		}
		return result;
	}
}
