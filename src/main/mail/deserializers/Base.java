package mail.deserializers;

import mail.deserializers.property.Properties;

public class Base {


	public static String DRIVER_NAME = new Properties().getPropValues("browser.driver","config.properties");
	public static String DRIVER_VERSION =  new Properties().getPropValues("driver.version","config.properties");
	public static String DRIVER_PATH =  new Properties().getPropValues("driver.path","config.properties");


}

