package mail.selenium;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import mail.ui.initdriver.Element;
import mail.ui.initdriver.InitialDriver;

public class SeleniumFactory {

    public static <T> T init(Class<T> type){
        return PageFactory.initElements(InitialDriver.currentDriver(), type);
    }

    public static WebElement getElement(By locator){
        return Element.getElementWait()
                .withMessage("Location of element " + locator)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static WebElement getElement(String pattern, Object... args){
        By locator = By.xpath(String.format(pattern, args));
        return Element.getElementWait()
                .withMessage("Location of element " + locator)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public static WebElement findElement(String pattern, Object... args){
        By locator = By.xpath(String.format(pattern, args));
        try {
            return InitialDriver.currentDriver().findElement(locator);
        }catch (NoSuchElementException ex){
            return null;
        }
    }

    public static WebElement findElement(WebElement element, String pattern, Object... args){
        By locator = By.xpath(String.format(pattern, args));
        try {
            return element.findElement(locator);
        }catch (NoSuchElementException ex){
            return null;
        }
    }

}
