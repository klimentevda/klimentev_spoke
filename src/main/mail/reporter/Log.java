package mail.reporter;

import org.apache.log4j.Logger;

public class Log {


private static Logger logger = Logger.getLogger("MyLoger");

    public static void debug(String message) {
        logger.debug(message);
    }

    public static void info(String message) {
        logger.info(message);
    }

    public static void error(String message) {
        logger.error(message);
    }
}
