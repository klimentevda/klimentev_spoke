package mail.utils;


import mail.ui.initdriver.InitialDriver;
import net.bytebuddy.utility.RandomString;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.testng.ITestResult;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import static java.lang.Math.abs;

public class UtilsMethods {
    private static final Random rnd = new Random(System.nanoTime());
    private WebDriver driverLocal= InitialDriver.getInstance().driver;
    private static UtilsMethods Instance;

    public static UtilsMethods getInstance()
    {
        synchronized (UtilsMethods.class)
        {
            if(Instance==null)
            {
                Instance= new UtilsMethods();
            }
        }
        return Instance;
    }

    public String screenFullPage(ITestResult arg)
    {
        try {
            final Screenshot screenshot = new AShot()
                    .shootingStrategy(ShootingStrategies.viewportPasting(100))
                    .takeScreenshot(driverLocal);
            final BufferedImage image = screenshot.getImage();
            String path="src\\resources\\ScreenShot";
            File file = new File(path);
            if(!file.exists())
                {
                  file.mkdir();
                }
            SimpleDateFormat format = new SimpleDateFormat("HH_mm_dd-MM-yyyy");
            String dateString = format.format( new Date()   );
            ImageIO.write(image, "PNG", new File(path+"\\"
                    + arg.getName()+"_"+dateString+".png"));
            return new String(Base64.encodeBase64(screenshot.toString().getBytes()), "UTF-8");
        }
        catch (IOException ex)
        {
            return  ex.getMessage();
        }
    }
    /**
     *Create Screen
     */
    public String createScreen()
    {
        try {
            WebDriver augmentedDriver = new Augmenter().augment(driverLocal);
            return "data:image/png;base64," + (((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.BASE64));
        }
        catch (NullPointerException ex)
        {
            return null;
        }
    }

    public String captureScreenToFile()
    {
        String path;
        try {
            WebDriver augmentedDriver = new Augmenter().augment(driverLocal);
            File source = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);
            path = "./target/screenshots/" + source.getName();
            FileUtils.copyFile(source, new File(path));
        }
        catch(IOException e) {
            path = "Failed to capture screenshot: " + e.getMessage();
        }
        return path;
    }

    public String genrateRandomName(int length)
    {
        String randomName="TEST "+ new RandomString().make(length);
        return  randomName;
    }
    public String uniqueNumbers(int size) {

        StringBuilder sb = new StringBuilder(size);

        while (sb.length() < size) {
            long l = abs(rnd.nextLong());
            sb.append(Long.toString(l));
        }

        String s = sb.toString();
        return s.substring(0, size);
    }
    public int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }


    public  static boolean moveFiles(File sourceFile, File destFile)
    {

        if (sourceFile.isDirectory())
        {
            for (File file : sourceFile.listFiles())
            {

                moveFiles(file, new File(file.getPath()));
            }
        }
        else
        {
            try {
                Files.move(Paths.get(sourceFile.getPath()), Paths.get(destFile.getPath()), StandardCopyOption.REPLACE_EXISTING);
                return true;
            } catch (IOException e) {
                return false;
            }
        }
        return false;


    }

    public static void renameFileUseJavaNewIO(String srcFilePath, String destFilePath)
    {
        try
        {
            if(srcFilePath!=null && srcFilePath.trim().length()>0 && destFilePath!=null && destFilePath.trim().length()>0)
            {
                /* Create the source Path instance. */
                Path srcPathObj = Paths.get(srcFilePath).toAbsolutePath();

                /* Create the target Path instance. */
                Path destPathObj = Paths.get(destFilePath+ "\\allure-results").toAbsolutePath();

                /* Rename source to target, replace it if target exist. */
                Path targetPathObj = Files.move(srcPathObj, destPathObj, StandardCopyOption.COPY_ATTRIBUTES);

                System.out.println("Use java new io to moveFiles success from " + srcFilePath + " to " + destFilePath);
            }
        }catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }
}