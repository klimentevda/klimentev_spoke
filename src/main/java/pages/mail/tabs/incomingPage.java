package java.pages.mail.tabs;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class incomingPage {

    @FindBy(id = "PH_user-email")
    private WebElement emailUserLink;

    public WebElement getEmailUserLink() {
        return emailUserLink;
    }
}
